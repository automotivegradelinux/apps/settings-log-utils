/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VIDEOUTILS_H
#define VIDEOUTILS_H

#include <memory>
#include <string>
#include <opencv2/videoio.hpp>

namespace VIDEO_UTILS
{
class VideoUtils
{
public:
	VideoUtils(int device, int framerate, int wid, int hght, std::string file, std::string co);
	~VideoUtils()
	{
	}
	void writeVideoFrame(const cv::UMat& f);
	void releaseSource();

private:
	std::string getCurrentTime();
	bool openVideoWriter();

public:
	bool isReady;
	std::unique_ptr<cv::VideoCapture> cvCapture;

private:
	int fps;
	int width;
	int height;
	int deviceno;
	int framecount;
	std::string codec;
	std::string filename;
	std::unique_ptr<cv::VideoWriter> cvWriter;
};
}

#endif // VIDEOUTILS_H
