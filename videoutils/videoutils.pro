TARGET = videoutils

LIBS += -lopencv_core -lopencv_videoio -lglib-2.0 -lopencv_imgproc

DESTDIR = $${OUT_PWD}/../package/root/bin

HEADERS += \
    videoutils.h

SOURCES += \
    videoutils.cpp

INCLUDEPATH += \
    $$[QT_SYSROOT]/usr/include/glib-2.0/ \
    $$[QT_SYSROOT]/usr/lib/glib-2.0/include

