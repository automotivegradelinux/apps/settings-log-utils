/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <linux/input.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

static int eventFd = -1;
static FILE* pfile;
struct input_event ev0[64];
static int screenWidth = 0;
static int screenHeight = 0;

extern char* getTouchScreenInfo();

static int handle_event()
{
	size_t i;
	ssize_t rd;
	char str[256];
	float x = 0, y = 0;

	rd = read(eventFd, ev0, sizeof(struct input_event)* 64);

	if(rd < 0)
                return -1;

	if((size_t)rd < sizeof(struct input_event))
		return 0;

	for(i = 0; i < ((size_t)rd) / sizeof(struct input_event); i++)
	{
		memset(str, 0, sizeof(str));

		if(ev0[i].type == 3)
		{
			if(ev0[i].code == 53 || ev0[i].code == 0)
			{//screen widthに対して変換
				x = (float)ev0[i].value / screenWidth;
				sprintf(str, "%2d\t%ld\t%ld\t%3hu\t%3hu\t%f\r\n",
						i, ev0[i].time.tv_sec, ev0[i].time.tv_usec, ev0[i].type, ev0[i].code, x);
			}
			else if(ev0[i].code == 54 || ev0[i].code == 1)
			{//screen　heightに対して変換
				y = (float)ev0[i].value / screenHeight;
				sprintf(str, "%2d\t%ld\t%ld\t%3hu\t%3hu\t%f\r\n",
						i, ev0[i].time.tv_sec, ev0[i].time.tv_usec, ev0[i].type, ev0[i].code, y);
			}
			else
			{
				sprintf(str, "%2d\t%ld\t%ld\t%3hu\t%3hu\t%d\r\n", i, ev0[i].time.tv_sec, ev0[i].time.tv_usec, ev0[i].type, ev0[i].code, ev0[i].value);
			}
		}
		else
		{
			sprintf(str, "%2d\t%ld\t%ld\t%3hu\t%3hu\t%d\r\n", i, ev0[i].time.tv_sec, ev0[i].time.tv_usec, ev0[i].type, ev0[i].code, ev0[i].value);
		}

		fputs(str, pfile);
	}
	fflush(pfile);
	return 1;
}

void printUsage(char* myName)
{
	printf("usage:\n \
	%s -f <string>\n \
	-f : file name for recording event\n", myName);
}

int main(int argc,char *argv[])
{
	int done = 1;
	int index = 0;
	char* fName = NULL;
	char eventNode[50];
	const char* tsInfo;

	if (argc < 3)
	{
		printUsage(argv[0]);
		return -1;
	}

	for (index = 1; index < argc; index++)
	{
		if (strcmp(argv[index], "-f") == 0)
		{
			fName = argv[++index];
		}
		else
		{
			printUsage(argv[0]);
			return -1;
		}
	}

	tsInfo = getTouchScreenInfo();
	if (NULL == tsInfo) {
		printf("Failed to get touch screen info.\n");
		return -1;
	}

	memset(eventNode, 0, sizeof(eventNode));
	sscanf(tsInfo, "%s %d %d", eventNode, &screenWidth, &screenHeight);

    eventFd = open(eventNode, O_RDWR);
	if(eventFd <0) {
		printf("open input device error\n");
		return -1;
	}

	pfile = fopen(fName, "wb+");
	if(pfile == NULL)
	{
		printf("Failed to create file!\n");
		return -1;
	}

	while (done)
	{
		printf("begin %s...\n", eventNode);
		done = handle_event();
		printf("end %s...\n", eventNode);
	}

	fclose(pfile);

	if(eventFd > 0)
	{
		close(eventFd);
		eventFd = -1;
	}

	return 0;
}
