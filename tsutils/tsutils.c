/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <dirent.h>
#include <linux/input.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#define LONG_BITS (sizeof(long) << 3)
#define NUM_LONGS(bits) (((bits) + LONG_BITS - 1) / LONG_BITS)
#define DEV_INPUT	"/dev/input"
#define LENGTH_TSDEVNODE (NAME_MAX + 1 + sizeof(DEV_INPUT))
#define LENGTH_TSINFO (LENGTH_TSDEVNODE + 2 + 11*2)

char tsInfo[LENGTH_TSINFO];

static inline int testBit(long bit, const long *array) {
    return (array[bit / LONG_BITS] >> bit % LONG_BITS) & 1;
}

char* getTouchScreenInfo() {
    DIR* dir;
    struct dirent* itemPtr;
    int fd;
    int isSingleTouch = 0, hasTouchScreen = 0;
    long absbits[NUM_LONGS(ABS_CNT)];
    char tsDevNode[LENGTH_TSDEVNODE];
    struct input_absinfo absInfo;
    int maxX, maxY;

    memset(tsDevNode, 0, sizeof(tsDevNode));
    memset(tsInfo, 0, sizeof(tsInfo));

    if ((dir = opendir(DEV_INPUT)) == NULL) {
        printf("open %s failed.\n", DEV_INPUT);
        return NULL;
    }

    while ((itemPtr = readdir(dir)) != NULL) {
        if ((strstr(itemPtr->d_name, "event") != NULL) && (2 == itemPtr->d_type)) {
            snprintf(tsDevNode, sizeof(tsDevNode), "%s/%s", DEV_INPUT, itemPtr->d_name);
            fd = open(tsDevNode, O_RDONLY);
            if (fd < 0) {
                printf("open %s failed.\n", tsDevNode);
                return NULL;
            }

            if (ioctl(fd, EVIOCGBIT(EV_ABS, sizeof(absbits)), absbits) >= 0) {
                isSingleTouch = !testBit(ABS_MT_POSITION_X, absbits);
            } else {
                close(fd);
                continue;
            }

            if (ioctl(fd, EVIOCGABS((isSingleTouch ? ABS_X : ABS_MT_POSITION_X)), &absInfo) >= 0) {
                maxX = absInfo.maximum;
            } else {
                close(fd);
                continue;
            }

            if (ioctl(fd, EVIOCGABS((isSingleTouch ? ABS_Y : ABS_MT_POSITION_Y)), &absInfo) >= 0) {
                maxY = absInfo.maximum;
            } else {
                close(fd);
                continue;
            }

            hasTouchScreen = 1;
            break;
        }
    }

    if (!hasTouchScreen) {
        return NULL;
    }

    snprintf(tsInfo, sizeof(tsInfo), "%s %d %d", tsDevNode, maxX, maxY);

    close(fd);
    closedir(dir);
    return tsInfo;
}
